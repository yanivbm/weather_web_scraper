"""
This file contains the script to display databases from the SQL server.
"""

import SQLConnect


def display_sql_db(host, user, password):
    """
    This function displays all databases that exist in a MySQL repository
    :param host: MySQL host name
    :param user: MySQL user name
    :param password: MySQL password
    """
    # Connect to DATABASE SERVER
    con, cur = SQLConnect.connect_to_server(host, user, password)

    # Show all databases available
    cur.execute("SHOW DATABASES")

    for db in cur:
        print(db)
