"""
This file contains supplementary utils for the scraping process.
Functions are related to:
-INPUT PARSING
-SCRAPING PROCESS MANAGMENT
-ADDITIONAL DATA RETREIVAL
"""

# Import general tools
import pandas as pd
import sys
import logging
import argparse
import os
import requests
import io
import zipfile
import time
from datetime import datetime

# Import other project files
import Constants
import SQLCreateDB
import SQLCreateTables
import SQLInsertToCity

logging.disable(logging.CRITICAL)


########################################### INPUT PARSING ###########################################
def parse_input_args():
    """
    Init parser for operations calculator that gets command line input.
    :return: parser object, parser args
    """
    parser = argparse.ArgumentParser(description="Weather web-scraper input. Please insert the following:\n"
                                                 "-type {Scraping module to use: "
                                                 "History/Forecast/Calendar/Cities/Countries}\n"
                                                 "--locations {Number of locations} - OPTIONAL \n"
                                                 "-chrome_canary {Google chrome canary .exe path} - "
                                                 "Only for History/Forecast Modules\n"
                                                 "-chrome_driver {Google chrome driver .exe path} - "
                                                 "Only for History/Forecast Modules\n"
                                                 "-mysql_host {Local MySQL Host name} - DEFAULT = localhost \n"
                                                 "-mysql_port {Local MySQL Port name} - DEFAULT = 3306 \n"
                                                 "-mysql_pass {Local MySQL Password}\n"
                                                 "-country {Country to fetch} - "
                                                 "OPTIONAL (Use only for History/Forecast)\n"
                                                 "-year_start {Fetching year range start} - OPTIONAL (Default = 2000)\n"
                                                 "-year_end {Fetching year range end} - "
                                                 "OPTIONAL (Default = current year)\n")
    parser.add_argument('-type', action='store', type=str,
                        help='Choose scraping type: Forecast/History/Countries/Cities/Calendar')
    parser.add_argument('-locations_amt', action='store', type=int,
                        help='Choose amount of locations to be scraped (OPTIONAL - If empty - All locations)')
    parser.add_argument('-chrome_canary', action='store', type=str,
                        help='Google chrome canary .exe pat')
    parser.add_argument('-chrome_driver', action='store', type=str,
                        help='Google chrome driver .exe path')
    parser.add_argument('-mysql_host', action='store', type=str, default='localhost',
                        help='MySQL host name (OPTIONAL - Supply only if different than localhost)')
    parser.add_argument('-mysql_port', action='store', type=str, default='3306',
                        help='MySQL port number (OPTIONAL - Supply only if different than 3306)')
    parser.add_argument('-mysql_user', action='store', type=str,
                        help='MySQL user name')
    parser.add_argument('-mysql_pass', action='store', type=str,
                        help='MySQL password')
    parser.add_argument('-country', action='store', type=str,
                        help='Country to fetch information for (OPTIONAL- If empty, fetch all countries')
    parser.add_argument('-year_start', action='store', type=int, default=2000,
                        help='Fetching year range start. (OPTIONAL - Default value = 2000)')
    parser.add_argument('-year_end', action='store', type=int, default=datetime.now().year,
                        help='Fetching year range end. (OPTIONAL - Default value = CURRENT YEAR')

    return parser, parser.parse_args()


# Parse input parameters
def validate_input_main(parser, args, browser=True, sql=True):
    """
    This function calls another function that validates user input.
    If input is valid, it is assigned to variables.
    :param parser: parser object
    :param args: input arguments list
    :param browser: Should browser input parameters be mandatory (Boolean)
    :param sql: Should sql input parameters be mandatory (Boolean)
    :return: Input variables as arguments - chrome_driver_path, chrome_canary_path, locations_amt
    """
    if not validate_input_sub(parser, args, browser=browser, sql=sql):
        sys.exit(1)
    else:
        chrome_driver_path = args.chrome_driver
        chrome_canary_path = args.chrome_canary
        if not args.locations_amt:
            locations_amt = None
        else:
            locations_amt = int(args.locations_amt)
        mysql_host = args.mysql_host
        mysql_port = args.mysql_port
        mysql_user = args.mysql_user
        mysql_pass = args.mysql_pass
        if args.country:
            country_to_fetch = args.country.capitalize()
        else:
            country_to_fetch = False
        year_start = args.year_start
        year_end = args.year_end
        return (chrome_driver_path, chrome_canary_path, locations_amt, mysql_host, mysql_port, mysql_user,
                mysql_pass, country_to_fetch, year_start, year_end)


def validate_input_sub(parser, args, browser=True, sql=True):
    """
    Validates input. Makes sure that files exist and locations amount is a valid integer.
    :return: is_valid (Boolean)
    """

    is_valid = True

    # Check for Missing arguments
    if (not args.chrome_canary or not args.chrome_driver) and browser:
        parser.error(Constants.INPUT_ERROR_MSG_BROWSER)
        is_valid = False
    if (not args.mysql_user or not args.mysql_pass) and sql:
        parser.error(Constants.INPUT_ERROR_MSG_SQL)
        is_valid = False

    # Google Chrome canary path is invalid
    if not os.path.isfile(args.chrome_canary):
        parser.error("Invalid path for Google Chrome Canary. File does not exist")
        is_valid = False
    # Google Chrome driver path is invalid
    if not os.path.isfile(args.chrome_driver):
        parser.error("Invalid path for Google Chrome driver. File does not exist")
        is_valid = False
    try:
        # Locations amount for retrieval is a negative number
        if int(args.locations_amt) <= 0:
            print("Invalid locations amount. Please enter an integer above 0")
            is_valid = False
    except ValueError:
        # Locations amount for retrieval is not an integer
        print('Invalid locations amount. Please enter an integer above 0')
        is_valid = False
    except TypeError:
        pass
    return is_valid


def init_db(mysql_host, mysql_user, mysql_pass):
    # Init DB
    SQLCreateDB.create_sql_db(mysql_host, mysql_user, mysql_pass)
    SQLCreateTables.create_sql_tables(mysql_host, mysql_user, mysql_pass)


########################################### SCRAPING PROCESS MANAGMENT ###########################################

def read_url_main(scraper, obs, airport):
    """
    This function calls other function which parses source html, and adds calls another function that adds
    information to errors table if an error had occurred.
    :param scraper: Scraper object instance
    :param obs: Forecast or History object instance
    :param airport: airport code (string)
    :return: BOOL
    """
    # Parse scrape_weather_history data table
    if not obs.get_data_table():
        scraper.fetch_errors.append(airport)
        print(f'Fetch errors: {scraper.fetch_errors}')
        return False
    # Parse page header
    if not obs.get_top_level_details():
        scraper.fetch_errors.append(airport)
        print(f'Fetch errors: {scraper.fetch_errors}')
        return False
    return True


def append_to_dataframe(scraper, obs, airport):
    """
    This function adds scraped information to the scrape_weather_history dataframe or adds information to errors table if an error
    had occurred during html source parsing process
    :param scraper: Scraper object instance
    :param obs: Forecast or History object instance
    :param airport: airport code (string)
    :return: BOOL
    """
    # Append data to scrape_weather_history Pandas dataframe
    try:
        print(f'Fetched data: \n{obs.data}')
        scraper.data = scraper.data.append(pd.DataFrame([obs.data], columns=obs.data.keys()), sort=False)
        return True
    except:
        scraper.write_errors = scraper.write_errors.append(airport)
        print(f'Write errors: {scraper.write_errors}')
        return False


########################################### ADDITIONAL DATA RETREIVAL ###########################################

def get_pollution_info(locations_df, locations_amt, mysql_host, mysql_user, mysql_pass, mysql_port):
    """
    This function retrieves pollution inofrmation (of nearest city) based on lat/lon coordinates, and writes to
    'cities' table.
    Information is retrieved via an API, and for the entire airports dataset.
    Writing to 'cities' table is done once for every fixed amount of API iterations, appending the newly updated data

    :param locations_df: Dataframe
    :return: Dataframe (Updated locations dataframe)
    """
    # Add new columns to dataframe
    locations_df[Constants.REGION] = ''
    locations_df[Constants.POLLUTION_AQIUS] = ''
    locations_df[Constants.POLLUTION_AQICN] = ''
    locations_df[Constants.POLLUTION_MAINUS] = ''
    locations_df[Constants.POLLUTION_MAINCN] = ''

    # Iterate on locations dataframe and retrieve pollution information for each location
    prev_sql_upload_ind = 0
    for ind, row in locations_df[:locations_amt].iterrows():
        print(row[Constants.AIRPORT_INPUT_COL_NAME], )
        time.sleep(10)
        params = {Constants.LAT: row[Constants.LAT], Constants.LON: row[Constants.LON],
                  Constants.KEY: Constants.POLLUTION_API_KEY, }
        r = requests.get(url=Constants.POLLUTION_API_ENDPOINT, params=params)
        info = r.json()
        try:
            row[Constants.CITY] = row[Constants.CITY].capitalize()
            row[Constants.REGION] = info[Constants.DATA][Constants.STATE]
            row[Constants.POLLUTION_AQIUS] = int(info[Constants.DATA][Constants.CURRENT][Constants.POLLUTION][
                Constants.AQIUS])
            row[Constants.POLLUTION_AQICN] = int(info[Constants.DATA][Constants.CURRENT][Constants.POLLUTION][
                Constants.AQICN])
            row[Constants.POLLUTION_MAINUS] = info[Constants.DATA][Constants.CURRENT][Constants.POLLUTION][
                Constants.MAINUS]
            row[Constants.POLLUTION_MAINCN] = info[Constants.DATA][Constants.CURRENT][Constants.POLLUTION][
                Constants.MAINCN]
            locations_df.loc[ind] = row

            # Update SQL table
            if ind % 10 == 0 and ind != 0:
                locations_df_temp = locations_df.iloc[prev_sql_upload_ind:ind + 1, :]
                SQLInsertToCity.insert_data_to_cities(locations_df_temp, mysql_host, mysql_user, mysql_pass, mysql_port)
                prev_sql_upload_ind = ind + 1
        except (KeyError , ValueError):
            try:
                print(info[Constants.DATA][Constants.MESSAGE])
                continue
            except KeyError:
                print(Constants.POLLUTION_DATA_MISSING_MSG)
                continue

    return locations_df


def get_csv_from_web_zip(url, csv_filename):
    """
    This function reads a csv that is inside a web zip file, and converts csv to dataframe.
    """
    # This function is in use in CountriesMain file
    r = requests.get(url=url)
    zf = zipfile.ZipFile(io.BytesIO(r.content))
    df = pd.read_csv(zf.open(csv_filename))
    df = df.rename(columns={"lat": "lat_2", "lng": "lon_2", "city": "city_2", "country": "country_2", "id": "id_2"})
    return df


def join_df(df1, df2, left_on, right_on, method):
    """
    This function joins two dataframes.
    """
    # This function is in use in CountriesMain file
    return df1.merge(df2, left_on=left_on, right_on=right_on, how=method)
