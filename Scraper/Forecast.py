"""
This file contains Forecast class.
This class handles data scraping (via BeautifulSoup) from html sources of Daily Forecast web pages (wunderground.com).
Website name is wunderground.com.
***This class is not involved with http request and connection to browsers.
"""

# Import scraping tools
import logging

# Import other project files
import Constants

logging.disable(logging.CRITICAL)


class Forecast:
    def __init__(self, calendar_html1, calendar_html2, airport_code):
        """
        Init Weather monthly calendar object instance
        :param data: Dictionary for storing retrieved data
        :param calendar_html1: Full source html of current month calendar
        :param calendar_html2: Full source html of next month calendar
        :param location/subheading: html parameters
        :param airport_code : Current retrieved location
        """
        self.data = {}
        self.calendar_html1 = calendar_html1
        self.calendar_html2 = calendar_html2
        self.location = None
        self.subheading = None
        self.airport_code = airport_code

    def get_data_table(self):
        """
        This method scraps the scrape_weather_history weather monthly calendar of a certain location.
        Data is saved in a dictionary that will be later appended to a pandas dataframe.
        :return: True/False based on operation success/failure
        """
        try:
            # Parse via BeautifulSoup
            date_list1 = self.calendar_html1.find_all(Constants.DIV, class_=Constants.CLASS_NAME_DATE)
            phrase_list1 = self.calendar_html1.find_all(Constants.DIV, class_=Constants.CLASS_NAME_PHRASE)
            temprature_list1 = self.calendar_html1.find_all(Constants.DIV, class_=Constants.CLASS_NAME_TEMPRATURE)
            temperature_scale = self.calendar_html1.find_all('a', class_='button selected')[0].get_text()
            self.data[Constants.CURRENT_DATE] = f'{Constants.CUR_DAY}/{Constants.CUR_MONTH}/{Constants.CUR_YEAR}'
            self.data[Constants.TEMPRATURE_SCALE] = temperature_scale
            self.data[Constants.AIRPORT_CODE] = self.airport_code
            skip = True

            # Iterate on calendar.
            # Retrieve data future data for today + 10 days.
            # Retrieve data from yesterday as 'final' weather per date
            # All data is retrieved to one line
            for d, p, t in zip(date_list1, phrase_list1, temprature_list1):
                d_num = int(d.get_text())
                if d_num == 1:
                    skip = False
                if skip:
                    continue
                else:

                    # Retrieve yesterday
                    if d_num == Constants.CUR_DAY - 1:
                        self.data[Constants.PREVIOUS_DATE] = f'{d_num}/{Constants.CUR_MONTH}/{Constants.CUR_YEAR}'
                        self.data[Constants.PREVIOUS_DATE_PHARSE] = p.get_text()
                        self.data[Constants.PREVIOUS_DATE_MAX_TEMP] = t.find_all(Constants.SPAN)[0].get_text()
                        self.data[Constants.PREVIOUS_DATE_MIN_TEMP] = t.find_all(Constants.SPAN)[2].get_text()

                    # Retrieve today + 10 days forward
                    elif Constants.CUR_DAY < d_num < (Constants.CUR_DAY + Constants.DAYS_TO_FORECAST):
                        self.data[
                            Constants.CURRENT_DATE] = f'{Constants.CUR_DAY}/{Constants.CUR_MONTH}/{Constants.CUR_YEAR}'
                        self.data[f'+{d_num - Constants.CUR_DAY} days date'] = \
                            f'{d_num}/{Constants.CUR_MONTH}/{Constants.CUR_YEAR}'
                        self.data[f'+{d_num - Constants.CUR_DAY} days phrase'] = p.get_text()
                        self.data[f'+{d_num - Constants.CUR_DAY} days max_temp'] = t.find_all(Constants.SPAN)[
                            0].get_text()
                        self.data[f'+{d_num - Constants.CUR_DAY} days min_temp'] = t.find_all(Constants.SPAN)[
                            2].get_text()
            return True
        except:
            return False

    def get_top_level_details(self):
        """
        This method scraps the header details of a monthly calendar of a certain airport (location).
        Data is saved in a dictionary that will be later appended to a pandas dataframe.
        :return: True/False based on operation success/failure
        """
        try:
            # Parse via BeautifulSoup
            self.subheading = self.calendar_html1.find_all(Constants.SPAN, class_=Constants.SUBHEADING)[
                0].get_text().split(',')
            for elem in self.subheading[:-2]:
                field_name = elem.split('\n')[1]
                field_data = elem.split('\n')[2].strip()
                if field_name:
                    self.data[field_name] = field_data
                else:
                    print(Constants.UNKNOWN_FIELD_MSG)

            # Retrieve information
            self.data[Constants.LATITUDE] = self.subheading[-2].strip()
            self.data[Constants.LONGITUDE] = self.subheading[-1].strip()
            self.location = self.calendar_html1.find_all(Constants.H1)[0].find_all(Constants.SPAN)[0].get_text().split(
                ',')
            self.data[Constants.STATION_NAME] = self.location[0].strip()
            self.data[Constants.REGION] = self.location[-2].strip()
            self.data[Constants.COUNTRY] = self.location[-1].strip().split(' ')[0].strip()
            return True
        except:
            return False
