"""
This file contains the script to display tables from the SQL database.
"""

import SQLConnect
import Constants as C


def display_sql_tables(host, user, password):
    """
    This function displays all tables that exist in a MySQL database
    :param host: MySQL host name
    :param user: MySQL user name
    :param password: MySQL password
    """

    # Connect to DATABASE SERVER
    con, cur = SQLConnect.connect_to_database(host, user, password, C.DB_Name)

    # Show all tables available
    cur.execute("SHOW TABLES")

    for table in cur:
        print(table)
