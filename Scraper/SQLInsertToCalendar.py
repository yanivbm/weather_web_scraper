"""
This file contains the script to fill in SQL table 'calendar'.
"""

from datetime import date, datetime, timedelta
import SQLConnect
import WeatherScraperUtils
import Constants

Y = 2000  # dummy leap year to allow input X-02-29 (leap day)


def get_season(now):
    """
    This function returns the season to which a date belongs to
    :param now: date
    :return: season as a string
    """
    seasons_range = [('winter', (date(Y, 1, 1), date(Y, 3, 20))),
                     ('spring', (date(Y, 3, 21), date(Y, 6, 20))),
                     ('summer', (date(Y, 6, 21), date(Y, 9, 22))),
                     ('autumn', (date(Y, 9, 23), date(Y, 12, 20))),
                     ('winter', (date(Y, 12, 21), date(Y, 12, 31)))]
    if isinstance(now, datetime):
        now = now.date()
    now = now.replace(year=Y)
    return next(season for season, (start, end) in seasons_range
                if start <= now <= end)


def get_list_datetime(start_year, end_year):
    """
    This function returns the list of dates in between two dates
    :param start_year: first date of our list
    :param end_year: last date of our list
    :return: list od dates
    """
    start_year = date(start_year, 1, 1)
    end_year = date(end_year, 12, 31)
    date_list = [start_year + timedelta(days=x) for x in range(abs((end_year - start_year).days))]
    return date_list


def insert_data_to_calendar(date_list, con, cur):
    """
    This function inserts data built from a list of dates to a SQL table
    :param date_list: list of dates
    :param con: connection to the database where the table we need to populate is in
    :param cur: cursor
    """

    for i in range(len(date_list)):
        # Get the values form scrape dictionnary
        date_formatted = date_list[i].strftime("%d-%m-%Y")
        season = get_season(date_list[i])
        year = date_list[i].year
        month = date_list[i].month
        day = date_list[i].day
        weekday = date_list[i].isoweekday()
        week_number = date_list[i].isocalendar()[1]

        sqlformula = """INSERT INTO calendar\
                    (date, season, year, month, day, weekday, week_number) VALUES (STR_TO_DATE(%s,'%d-%m-%Y'), %s, %s, 
                    %s, %s, %s, %s)
                    """
        history_row = (date_formatted, season, year, month, day, weekday, week_number)

        cur.execute(sqlformula, history_row)

    con.commit()
    cur.close()


def generate_calendar(parser, args):
    """
    This function handles the pre-processing required for calendar table generation.
    Count
    :param parser: input parameters parser
    :param args: input arguments
    """
    # Parse input
    (chrome_driver_path, chrome_canary_path, locations_amt, mysql_host, mysql_port, mysql_user,
     mysql_pass, country_to_fetch, year_start, year_end) = \
        WeatherScraperUtils.validate_input_main(parser, args, browser=False, sql=True)

    # Init SQL
    WeatherScraperUtils.init_db(mysql_host, mysql_user, mysql_pass)
    con, cur = SQLConnect.connect_to_database(mysql_host, mysql_user, mysql_pass, Constants.DB_Name)

    # Build table
    date_list = get_list_datetime(Constants.CALENDAR_START_YEAR, Constants.CALENDAR_END_YEAR)
    insert_data_to_calendar(date_list, con, cur)
