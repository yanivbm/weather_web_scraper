"""
This file contains a script for locations table generation.
"""

from Scraper import Scraper
import SQLInsertToCity
import WeatherScraperUtils
import Constants


def build_locations_table(mysql_host, mysql_user, mysql_pass, mysql_port, locations_amt):
    """
    Intial input is 'airport.csv' file that exists in BitBucket.
    File is enriched with an api for retrieving pollution measurements, and with a web zip file for getting the
    population of each city.
    :return: scraper.locations (dataframe)
    """
    # Init scraper object for initial countries file read
    scraper = Scraper()
    scraper.read_input_file()

    # Enrich file with pollution and population information
    # Remove duplicate city records from zip csv, and convert all names to lowercase

    cities_population = WeatherScraperUtils.get_csv_from_web_zip(Constants.INPUT_COUNTRIES_ZIP,
                                                                 Constants.INPUT_COUNTRIES_ZIP_CSV)
    cities_population[Constants.CITY_ASCII] = cities_population[Constants.CITY_ASCII].apply(lambda x: x.lower())
    cities_population = cities_population.groupby(Constants.CITY_ASCII).first().reset_index()

    # Join by city name
    scraper.locations = WeatherScraperUtils.join_df(scraper.locations, cities_population, Constants.CITY,
                                                    Constants.CITY_ASCII, 'left')

    # Get pollution information through API and write to 'cities' SQL table
    scraper.locations = WeatherScraperUtils.get_pollution_info(scraper.locations, locations_amt,
                                                               mysql_host, mysql_user, mysql_pass, mysql_port)
    return scraper.locations


def cities_table_handler(parser, args):
    """
    This function handles the pre-processing required for cities table generation.
    Count
    :param parser: input parameters parser
    :param args: input arguments
    """
    # Parse input
    (chrome_driver_path, chrome_canary_path, locations_amt, mysql_host, mysql_port, mysql_user,
     mysql_pass, country_to_fetch, year_start, year_end) = \
        WeatherScraperUtils.validate_input_main(parser, args, browser=False, sql=True)

    # Init DB
    WeatherScraperUtils.init_db(mysql_host, mysql_user, mysql_pass)

    # Build table
    build_locations_table(mysql_host, mysql_user, mysql_pass, mysql_port, locations_amt)
