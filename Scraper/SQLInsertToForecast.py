"""
This file contains the script to fill in SQL table forecast.
"""
import Constants


def insert_data_to_forecast(sample_dict, con, cur, scarper):
    """
    This function inserts the forecast data in a specific table
    :param sample_dict: dictionary of forecast data
    :param con: connection to the database where the table we need to populate is in
    :param cur: cursor
    """
    try:
        cur.execute(f"""INSERT INTO forecast
                            (forecast_date, airport_code, temperature_scale, actual_max_temp, 
                            actual_min_temp, actual_conditions)
                        VALUES
                            (STR_TO_DATE(%s,'%d/%m/%Y'), %s, %s, %s, %s, %s)
                        ON DUPLICATE KEY UPDATE
                            actual_max_temp = %s,
                            actual_min_temp = %s,
                            actual_conditions = %s;""",
                    (sample_dict[Constants.PREVIOUS_DATE], sample_dict['Airport Code'],
                     sample_dict[Constants.TEMPRATURE_SCALE],
                     float(str(sample_dict[Constants.PREVIOUS_DATE_MAX_TEMP][:-1])),
                     float(str(sample_dict[Constants.PREVIOUS_DATE_MIN_TEMP][:-1])),
                     sample_dict[Constants.PREVIOUS_DATE_PHARSE],
                     float(str(sample_dict[Constants.PREVIOUS_DATE_MAX_TEMP][:-1])),
                     float(str(sample_dict[Constants.PREVIOUS_DATE_MIN_TEMP][:-1])),
                     sample_dict[Constants.PREVIOUS_DATE_PHARSE]
                     ))
        con.commit()
        for i in range(1, 10):
            cur_date = sample_dict[f'+{i} days date']
            cur.execute(f"""INSERT INTO forecast
                    (forecast_date, airport_code, temperature_scale, forecast_{i}_temp, forecast_{i}_conditions, last_update)
                VALUES
                    (STR_TO_DATE(%s,'%d/%m/%Y'), %s, %s, %s, %s, NOW())
                ON DUPLICATE KEY UPDATE
                    forecast_{i}_temp = %s,
                    forecast_{i}_conditions = %s,
                    last_update = NOW();""",
                        (cur_date, sample_dict['Airport Code'], sample_dict[Constants.TEMPRATURE_SCALE],
                         float(str(sample_dict[f'+{i} days max_temp'][:-1])),
                         sample_dict[f'+{i} days phrase'], float(str(sample_dict[f'+{i} days max_temp'][:-1])),
                         sample_dict[f'+{i} days phrase']))
            con.commit()
        return True

    except KeyError:
        return False
