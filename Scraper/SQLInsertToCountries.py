"""
This file contains a script for countries table generation.
Countries table needs to be generated once.
"""

import WeatherScraperUtils
import Constants
import SQLConnect
import requests


def build_countries_table(mysql_host, mysql_user, mysql_pass):
    """
    This function reads countries table from api, parses JSON and writes to 'countries' SQL table
    """

    # Read countries JSON from an API
    con, cur = SQLConnect.connect_to_database(mysql_host, mysql_user, mysql_pass, Constants.DB_Name)
    r = requests.get(url=Constants.COUNTRIES_ENDPOINT)
    countries = r.json()

    # Parse JSON and write to SQL
    for country in countries:
        if not country['population'] or not country['area'] or country['area'] == 0:
            density = 0
        else:
            density = country[Constants.POPULATION] / country[Constants.AREA]
        sqlformula = """INSERT INTO countries
                    (country_name, country_code, continent, region, population, area, density, gini) 
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                    """
        history_row = (country[Constants.NAME], country[Constants.ALPHA2CODE], country[Constants.REGION],
                       country[Constants.SUBREGION],
                       country[Constants.POPULATION],
                       country[Constants.AREA], density, country[Constants.GINI])

        cur.execute(sqlformula, history_row)
    con.commit()
    cur.close()


def countries_table_handler(parser, args):
    """
    This function handles the pre-processing required for countries table generation.
    Count
    :param parser: input parameters parser
    :param args: input arguments
    """
    # Parse input
    (chrome_driver_path, chrome_canary_path, locations_amt, mysql_host, mysql_port, mysql_user,
     mysql_pass, country_to_fetch, year_start, year_end) = \
        WeatherScraperUtils.validate_input_main(parser, args, browser=False, sql=True)

    # Init SQL
    WeatherScraperUtils.init_db(mysql_host, mysql_user, mysql_pass)

    # Build table
    build_countries_table(mysql_host, mysql_user, mysql_pass)
