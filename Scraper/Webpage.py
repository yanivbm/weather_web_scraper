"""
This file contains the Webpage class.
Webpage class handles:
-Connection to url
-Webpages source html retrieval (Including crawling between webpages)
- HTMLs are saved in a dictionary for later parsing (Parsing is done after disconnection from webpage)
"""
# Import scraping tools
import selenium
from http_request_randomizer.requests.useragent.userAgent import UserAgentManager
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

# Import other tools
import os
import time
import random
import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil import rrule

# Import other project files
import Constants

logging.disable(logging.CRITICAL)


class Webpage:
    def __init__(self, url, airport, year_start, year_end):
        """
        Inits webpage object.
        :param url: Webpage url
        :param airport: Airport ICAO code (Location to pull weather information for)
        Webpage object holds HTTP parameters, html source containers and general instance attributes
        """
        # HTTP parameters
        self.url = url
        print(self.url)
        self.ua = UserAgentManager()
        self.cur_ua = None
        self.page_source_calendar1 = None
        self.page_source_calendar2 = None
        self.driver = None
        self.action = None

        # HTML Source containers
        self.page_source = None
        self.page_source_data_table = {}
        self.data = {}

        # Other instance attributes
        self.airport_code = airport
        self.year_start = datetime(year_start, 1, 1)
        self.year_end = datetime(year_end, 12, 1)

    def access_url(self, scraper, cur_location, chrome_canary_path, chrome_driver_path):
        """
        This method calls anothe method that accesses URL through an HTTP request.
        :param scraper: Scraper object
        :param cur_location: Current location to fetch information for
        :param chrome_canary_path: Path of Google Chrome Canary .exe file
        :param chrome_driver_path: Path of Google Chrome driver .exe file
        :return: BOOL
        """

        # Get HTML source for each year-month in current airport
        success = False
        count_reps = 0
        while not success:
            count_reps += 1
            self.cur_ua = self.ua.get_random_user_agent()

            # Reload proxy list
            if len(scraper.proxy_list) == 0:
                scraper.get_proxy_list()

            # If MAX_REPS_PER_URL retries reached, break and url to errors_dict
            if count_reps > Constants.MAX_REPS_PER_URL:
                scraper.url_errors.append(cur_location)
                print(f'URL errors: {scraper.url_errors}')
                return False
            else:
                print(f'Trying to fetch airport {self.airport_code}, attempt # {count_reps}')

                # Invoke http GET request
                proxy = random.choice(scraper.proxy_list)
                if not self.connect_url(chrome_canary_path, chrome_driver_path):
                    scraper.proxy_list.remove(proxy)
                else:
                    return True

    def connect_url(self, chrome_canary_path, chrome_driver_path):
        """
        This function invokes a HTTP GET request and receives a html of a monthly weather calendar in a
        certain location.
        Http request uses random user agent, proxy and random mouse movements to reduce the odds of being classifed
        as spam and getting blocked by the scraped website.
        Then, function crawls to the following month calendar as well.
        :return: True/False based on operation success/failure
        """

        # HTTP request properties
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        # chrome_options.add_argument(f'--proxy-server={proxy}')
        chrome_options.add_argument('--ignore-certificate-errors')
        chrome_options.add_argument(f'user-agent={self.cur_ua}')
        chrome_options.binary_location = chrome_canary_path
        self.driver = webdriver.Chrome(executable_path=os.path.abspath(chrome_driver_path),
                                       chrome_options=chrome_options)

        # Enabling mouse movement
        self.action = ActionChains(self.driver)

        # HTTP GET request
        time.sleep(2)
        self.driver.get(self.url)
        time.sleep(7)

        # Check that a valid html page was received (We will find a better method in the future)
        if len(self.driver.page_source) < 100:
            return False
        else:
            return True

    def get_source_forecast(self, scraper):
        """
        This method gets the source html of a monthly temprature forecast page.
        Certain primary actions are taken in order to get all the required information
        :param scraper: Scraper object
        :return: BOOL
        """
        try:
            # Press on settings button to check what is the presented temperature scale
            self.driver.find_element_by_xpath(f"//button[@class='wu-settings']").click()
            time.sleep(1)

            # Retrieve HTML
            self.page_source_calendar1 = BeautifulSoup(self.driver.page_source, 'html.parser')

            # If HTML is unreasonably short, retrieval was unsuccessful and this url will be added to errors list
            if not (self.page_source_calendar1.find_all(Constants.UL, class_=Constants.CALENDAR_DAYS)) or \
                    len(self.page_source_calendar1.find_all(Constants.UL, class_=Constants.CALENDAR_DAYS)[
                            0].get_text().strip()) < 30:
                scraper.missing_data.append(self.airport_code)
                return False

            y = (datetime.now() + relativedelta(months=1)).year
            m = (datetime.now() + relativedelta(months=1)).month
            self.move_mouse_randomly()
            self.page_source_calendar2 = BeautifulSoup(self.driver.page_source, 'html.parser')
            if not (self.page_source_calendar2.find_all(Constants.UL, class_=Constants.CALENDAR_DAYS)) or \
                    len(self.page_source_calendar2.find_all(Constants.UL, class_=Constants.CALENDAR_DAYS)[
                            0].get_text().strip()) < 30:
                scraper.missing_data.append(self.airport_code)
                return False
            return True

        # Disconnect from URL (Close webpage)
        finally:
            # Exit webpage once completed
            self.driver.close()

    def get_source_history(self, scraper):
        """
        This method gets the source html of a monthly temprature history page.
        Certain primary actions are taken in order to get all the required information
        :param scraper: Scraper object
        :return: BOOL
        """
        try:
            # Crawl through monthly pages for certain location (airport)
            for ym in rrule.rrule(rrule.MONTHLY, dtstart=self.year_start, until=self.year_end):
                y = ym.year
                m = ym.month
                print(f'{self.airport_code}-{y}-{m}')
                # Move to next month if not processing first month (crawl by pressing on buttons)
                if ym > self.year_start:
                    # Random mouse movement to reduce the chance of being classified as spam
                    self.move_mouse_randomly()

                    self.driver.find_element_by_xpath(f"//option[@value='{Constants.CUR_YEAR - y}: {y}']").click()
                    time.sleep(1)
                    self.move_mouse_randomly()

                    self.driver.find_element_by_xpath(f"//option[@value='{m}']").click()
                    time.sleep(1)
                    self.move_mouse_randomly()

                    self.driver.find_element_by_xpath("//input[@value='View']").click()
                    time.sleep(7)

                # If an error occurred, add to missing data list and continue

                # Press on settings button to check what is the presented temperature scale
                self.move_mouse_randomly()
                self.driver.find_element_by_xpath(f"//button[@class='wu-settings']").click()
                time.sleep(1)

                # If HTML is unreasonably short, retrieval was unsuccessful and this url will be added to errors
                # list
                self.page_source = BeautifulSoup(self.driver.page_source, 'html.parser')
                if not (self.page_source.find_all(Constants.DIV, class_=Constants.SUMMARY_TABLE)) or \
                        len(self.page_source.find_all(Constants.DIV, class_=Constants.SUMMARY_TABLE)[
                                0].get_text().strip()) < 30:
                    scraper.missing_data.append(f'{self.airport_code}-{y}-{m}')
                    continue
                else:
                    # Save all HTML Sources (of each month) to a dictionary, ready for parsing after
                    # disconnection from webpage
                    self.page_source_data_table[f'{y}-{m}'] = self.page_source
                time.sleep(2)
                # except:
                #     scraper.missing_data.append(f'{self.airport_code}-{y}-{m}')
                #     continue
            return True
        except:
            return True
        finally:
            # Exit webpage once completed
            self.driver.close()

    def move_mouse_randomly(self):
        # Random mouse movement to reduce the chance of being classified as spam
        try:
            self.action.move_by_offset(random.randrange(Constants.RAND_RANGE_OFFSET),
                                       random.randrange(Constants.RAND_RANGE_OFFSET))
            self.action.perform()
            return True
        except selenium.common.exceptions.MoveTargetOutOfBoundsException:
            return True
