"""
This file contains main() function, that calls all other modules of this project
"""

import WeatherScraperUtils
import WeatherHistoryScraper
import DailyForecastScraper
import CitiesScraper
import SQLInsertToCalendar
import SQLInsertToCountries
import sys
import Constants
import logging.config
import logging


def main():
    """
    This function runs input parser function, and calls the relevant module based on the specified module in
    '-type' input parameter
    :return:
    """
    (parser, args) = WeatherScraperUtils.parse_input_args()

    if not args.type:
        print(Constants.PROCESS_TYPE_MSG)
        sys.exit(1)
    else:
        process_type = args.type.capitalize()
        # Scrape weather history
        if process_type == Constants.HISTORY:
            WeatherHistoryScraper.scrape_weather_history(parser, args)
        # Scrape weather forecast
        elif process_type == Constants.FORECAST:
            DailyForecastScraper.scrape_daily_forecast(parser, args)
        # Build cities table from api and web zip file
        elif process_type == Constants.CITIES:
            CitiesScraper.cities_table_handler(parser, args)
        # Build cities table from api
        elif process_type == Constants.COUNTRIES:
            SQLInsertToCountries.countries_table_handler(parser, args)
        elif process_type == Constants.CALENDAR:
            SQLInsertToCalendar.generate_calendar(parser, args)
        else:
            # Unrecognized process type
            logging.info(Constants.PROCESS_TYPE_MSG)
            sys.exit(1)


if __name__ == '__main__':
    main()
