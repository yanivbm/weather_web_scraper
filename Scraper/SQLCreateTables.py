"""
This file contains the script to create tables into the SQL database.
"""

import SQLConnect
import Constants as C


def create_sql_tables(host, user, password):
    """
    This function creates all tables in 'weather' database in mysql.
    If certain table already exist, operation is skipped
    :param host: MySQL host name
    :param user: MySQL user name
    :param password: MySQL password
    """
    # Connect to DATABASE SERVER
    con, cur = SQLConnect.connect_to_database(host, user, password, C.DB_Name)

    # Create Tables in the database

    # Create countries table
    cur.execute("""CREATE TABLE IF NOT EXISTS countries 
                         (country_name VARCHAR(80),
                         country_code VARCHAR(5) PRIMARY KEY,
                         continent VARCHAR(10),
                         region VARCHAR(30),
                         population INTEGER,
                         area FLOAT,
                         density FLOAT,
                         gini FLOAT,
                         last_update DATETIME DEFAULT NOW())""")

    # Create cities table
    cur.execute("""CREATE TABLE IF NOT EXISTS cities
                     (airport_code VARCHAR(5) PRIMARY KEY,
                     country_code VARCHAR(50),
                     region VARCHAR(50),
                     station_name VARCHAR(50),
                     longitude FLOAT,
                     latitude FLOAT,
                     pollution_rate_us VARCHAR(5),
                     pollution_source_us VARCHAR(2),
                     pollution_rate_cn VARCHAR(5),
                     pollution_source_cn VARCHAR(2),
                     last_update DATETIME DEFAULT NOW())""")


    # Create calendar table
    cur.execute("""CREATE TABLE IF NOT EXISTS calendar
                     (date DATE PRIMARY KEY,
                     season VARCHAR(10),
                     year INTEGER(4),
                     month INTEGER(2),
                     day INTEGER(2),
                     weekday INTEGER(1),
                     week_number INTEGER(2),
                     last_update DATETIME DEFAULT NOW())""")

    # Create history table
    cur.execute("""CREATE TABLE IF NOT EXISTS history
                     (date DATE,
                     airport_code VARCHAR(5),
                     temperature_scale VARCHAR(5),
                     temp_max_max FLOAT,
                     temp_max_min FLOAT,
                     temp_max_avg FLOAT,
                     temp_min_max FLOAT,
                     temp_min_min FLOAT,
                     temp_min_avg FLOAT,
                     temp_avg_max FLOAT,
                     temp_avg_min FLOAT,
                     temp_avg_avg FLOAT,
                     windspeed_max FLOAT,
                     windspeed_min FLOAT,
                     windspeed_avg FLOAT,
                     gust_wind_max FLOAT,
                     gust_wind_min FLOAT,
                     gust_wind_avg FLOAT,
                     pressure_sealevel_max FLOAT,
                     pressure_sealevel_min FLOAT,
                     pressure_sealevel_avg FLOAT,
                     last_update DATETIME DEFAULT NOW(),
                     PRIMARY KEY (date,airport_code)) """)

    # Create forecast table
    cur.execute("""CREATE TABLE IF NOT EXISTS forecast
                     (airport_code VARCHAR(5),
                     forecast_date DATE,
                     temperature_scale VARCHAR(5),
                     actual_max_temp FLOAT,
                     actual_min_temp FLOAT,
                     actual_conditions VARCHAR(30),
                     forecast_1_temp FLOAT,
                     forecast_2_temp FLOAT,
                     forecast_3_temp FLOAT,
                     forecast_4_temp FLOAT,
                     forecast_5_temp FLOAT,
                     forecast_6_temp FLOAT,
                     forecast_7_temp FLOAT,
                     forecast_8_temp FLOAT,
                     forecast_9_temp FLOAT,
                     forecast_10_temp FLOAT,
                     forecast_1_conditions VARCHAR(30),
                     forecast_2_conditions VARCHAR(30),
                     forecast_3_conditions VARCHAR(30),
                     forecast_4_conditions VARCHAR(30),
                     forecast_5_conditions VARCHAR(30),
                     forecast_6_conditions VARCHAR(30),
                     forecast_7_conditions VARCHAR(30),
                     forecast_8_conditions VARCHAR(30),
                     forecast_9_conditions VARCHAR(30),
                     forecast_10_conditions VARCHAR(30),
                     last_update DATETIME DEFAULT NOW(),
                     PRIMARY KEY (forecast_date,airport_code))""")

    # Commit and close connection
    con.commit()
    cur.close()
