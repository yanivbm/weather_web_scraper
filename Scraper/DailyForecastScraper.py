"""
This file contains the scrape_weather_history script of Daily Forecast web-scrapaing (wunderground.com).
The script calls related functions and runs the scrape_weather_history loop that iterates over all locations.
"""

# Import scraping tools
import logging

# Import other project files
from Scraper import Scraper
from Webpage import Webpage
from Forecast import Forecast
import WeatherScraperUtils
import Constants
import SQLInsertToForecast
import SQLConnect

logging.disable(logging.CRITICAL)


def scrape_daily_forecast(parser, args):
    """
    his function manages the process and calls other functions that perform the scraping.
    This function calls function that do the following: get proxy list, read locations from a csv file,
    init http GET request and scrape data from the html.
    """
    (chrome_driver_path, chrome_canary_path, locations_amt, mysql_host, mysql_port, mysql_user,
     mysql_pass, country_to_fetch, year_start, year_end) = \
        WeatherScraperUtils.validate_input_main(parser, args, browser=True, sql=True)

    # Init DB
    WeatherScraperUtils.init_db(mysql_host, mysql_user, mysql_pass)

    # Init Scraper object
    scraper = Scraper(country_to_fetch)

    # Retrieve available proxies list
    scraper.get_proxy_list()

    # Read input fle
    scraper.read_input_file()

    # Iterate over airports list and get the all monthly htmls for each location
    con, cur = SQLConnect.connect_to_database(mysql_host, mysql_user, mysql_pass, Constants.DB_Name)
    try:
        for ind, airport in enumerate(scraper.airport_list[:locations_amt]):

            scraper.check_status(ind)
            url = f'https://www.wunderground.com/calendar/{airport}'
            req = Webpage(url, airport, year_start, year_end)

            # Init Webpage object

            if not req.access_url(scraper, airport, chrome_canary_path, chrome_driver_path):
                continue
            req.get_source_forecast(scraper)

            # Scrape details from HTML
            forc = Forecast(req.page_source_calendar1, req.page_source_calendar2, req.airport_code)
            if not WeatherScraperUtils.read_url_main(scraper, forc, airport):
                continue

            # Update SQL table
            SQLInsertToForecast.insert_data_to_forecast(forc.data, con, cur, scraper)

        scraper.print_summary()
    finally:
        con.commit()
        cur.close()
