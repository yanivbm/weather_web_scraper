"""
This file contains Scraper class.
Scraper class handles the highest-level opeaation of the scraping process, and stores summary information
related to the general scraping process.
This class also handles proxies retrieval and proxies information storing.
"""

# Import scraping tools
from http_request_randomizer.requests.proxy.requestProxy import RequestProxy

# Import other tools
import pandas as pd
import sys
import time
import logging

# Import other project files
import Constants

logging.disable(logging.CRITICAL)


class Scraper:
    """
    Scraper object. This object handles a scraping process and holds data that relates to the entire process
    (e.g. Multiple URLs)
    """

    def __init__(self, country=False):
        """
        Inits a Scraper object with summary objects:
        - Error summary lists/dictionaries
        - self.locations - A dataframe intended for storing the entire input file (airports.csv)
        - self.airport_list - Input list of airport codes for iterating over
        - self.data - Dataframe to store the scraped data
        - Current proxy, available proxy list and proxy last update timestamp
        """
        self.url_errors = []
        self.fetch_errors = []
        self.write_errors = []
        self.missing_data = []
        self.locations = pd.DataFrame()
        self.airport_list = []
        self.data = pd.DataFrame()
        self.proxy_last_update = time.time()
        self.proxy_list = []
        self.proxy = ''
        self.country = country

    def get_proxy_list(self):
        """
        This method retrieves an available proxy list via an external package, and stores last update timestamp
        """
        try:
            req_proxy = RequestProxy()
        except:
            print(Constants.PROXY_ERROR_MSG)
            sys.exit(1)
        self.proxy_list = list(map(lambda x: x.get_address(), req_proxy.get_proxy_list()))
        self.proxy_last_update = time.time()

    def read_input_file(self):
        """
        This method parses locations input file. The following actions are taken:
        - locations with non-ascii characters are removed
        - City column is converted to lowercase (City will be used as a Key in CountriesMain file)
        - Airport list is built (Main scraper will iterate over this list)
        :return:
        """
        try:
            # Read a list of airport codes from a local csv
            self.locations = pd.read_csv(Constants.INPUT_FILENAME)
            self.locations = self.locations.groupby('city').first().reset_index()

            # Remove records with non-ascii characters
            self.locations = self.locations[
                self.locations.name.apply(lambda x: True if all(ord(char) < 128 for char in x) else False) &
                self.locations.country.apply(lambda x: True if all(ord(char) < 128 for char in x) else False) &
                self.locations.city.apply(lambda x: True if all(ord(char) < 128 for char in x) else False)]
            if self.country:
                self.locations = self.locations[
                    self.locations.country.apply(lambda x: True if x == self.country else False)]

            # Convert city to lowercase (City will be used as a Key in CountriesMain file)
            self.locations['city'] = self.locations.city.apply(lambda x: x.lower())
            self.locations = self.locations.sort_values(by=['id'])
            # Build airport list (Main scraper will iterate over this list)
            self.airport_list = list(self.locations['code2'])

        # Error handling
        except KeyError:
            print(Constants.MISSING_COLUMN_MSG)
            sys.exit(1)
        except FileNotFoundError:
            print(Constants.FILE_NOT_FOUND_MSG)
            sys.exit(1)
        if not self.airport_list:
            print(Constants.EMPTY_COLUMN_MSG)
            sys.exit(1)

    def check_status(self, ind):
        """
        This method checks the amount of errors before moving to scrape the next webpage.
        If amount of errors is beyond a certain limit, process is stopped.
        """

        # Terminate process if there are too many errors
        if len(self.url_errors) + len(self.fetch_errors) + len(self.url_errors) > (
                ind + 1) * Constants.ERRORS_RATE and ind > Constants.ERRORS_MIN_TERMINATE:
            print(Constants.TOO_MANY_ERRORS_MSG)
            self.print_summary()
            sys.exit(1)
        if time.time() - self.proxy_last_update > Constants.REFRESH_PROXY_MINS * Constants.SECS_PER_MIN:
            self.get_proxy_list()

    def print_summary(self):
        """
        This method prints summary objects (data and error lists)
        :return:
        """

        # Print summary
        print('\nSummary:\n')
        print(f'URL errors: {self.url_errors}')
        print(f'Fetching errors: {self.fetch_errors}')
        print(f'Writing errors: {self.write_errors}')
        print(f'Missing Data: {self.missing_data}')
