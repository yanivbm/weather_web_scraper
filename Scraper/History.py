"""
This file contains History class.
This class handles data scraping (via BeautifulSoup) from html sources of Weather History web pages
Website name is wunderground.com.
"""

# Import other project files
import Constants


class History:
    def __init__(self, year_month, source_html, airport_code):
        """
        Init Weather monthly calendar object instance
        :param data: Dictionary for storing retrieved data
        :param calendar_html1: Full source html of current month calendar
        :param calendar_html2: Full source html of next month calendar
        :param location/subheading: html parameters
        :param airport_code : Current retrieved location
        """
        self.data = {Constants.YEAR_MONTH: year_month}
        self.source_html = source_html
        self.data_table = None
        self.subheading = None
        self.location = None
        self.airport_code = airport_code

    def get_data_table(self):
        """
        This method scraps the scrape_weather_history weather data table from a monthly webpage of a certain location.
        Data is saved in a dictionary that will be later appended to a pandas dataframe.
        :return: True/False based on operation success/failure
        """
        try:
            # Parse via BeautifulSoup
            self.source_html.find_all(Constants.DIV)
            self.data_table = self.source_html.find_all(Constants.DIV, class_=Constants.SUMMARY_TABLE)[0].find_all(
                Constants.TR)
            temperature_scale = self.source_html.find_all('a', class_='button selected')[0].get_text().strip()

            # Scrape information from the scrape_weather_history data table
            for ind, line in enumerate(self.data_table):
                if not line.find_all(Constants.SVG):
                    row_name = line.find_all(Constants.TH)[0].get_text().replace(' ', '_').lower()
                    row_max = line.find_all(Constants.TD)[Constants.MAX_IND].get_text()
                    row_avg = line.find_all(Constants.TD)[Constants.AVG_IND].get_text()
                    row_min = line.find_all(Constants.TD)[Constants.MIN_IND].get_text()
                    self.data[row_name + '_max'] = row_max
                    self.data[row_name + '_min'] = row_min
                    self.data[row_name + '_avg'] = row_avg
                    self.data[Constants.TEMPRATURE_SCALE] = temperature_scale
                    self.data[Constants.AIRPORT_CODE] = self.airport_code
            return True
        except:
            return False

    def get_top_level_details(self):
        """
        This method scraps the header details of a monthly webpage of a certain airport (location).
        Data is saved in a dictionary that will be later appended to a pandas dataframe.
        :return: True/False based on operation success/failure
        """
        try:
            # Parse via BeautifulSoup
            self.subheading = self.source_html.find_all('span', class_='subheading')[0].get_text().split(',')
            for elem in self.subheading[:-2]:
                field_name = elem.split('\n')[1]
                field_data = elem.split('\n')[2].strip()
                if field_name:
                    self.data[field_name] = field_data
                else:
                    print(Constants.UNKNOWN_FIELD_MSG)

            # Retrieve information
            self.data[Constants.LATITUDE] = self.subheading[-2].strip()
            self.data[Constants.LONGITUDE] = self.subheading[-1].strip()
            self.location = self.source_html.find_all(Constants.H1)[0].find_all(Constants.SPAN)[0].get_text().split(
                ',')
            self.data[Constants.STATION_NAME] = self.location[0].strip()
            self.data[Constants.REGION] = self.location[-2].strip()
            self.data[Constants.COUNTRY] = self.location[-1].strip().split(' ')[0].strip()
            return True
        except:
            return False
