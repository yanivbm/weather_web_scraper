"""
This file contains all constants related to wunderground.com web-scraping project
"""
from datetime import date, datetime

# Datetime Constants
CUR_YEAR = datetime.now().year
CUR_MONTH = datetime.now().month
CUR_DAY = datetime.now().day
START_DATE = datetime(2018, 1, 1)
END_DATE = datetime(2018, 3, 1)
# END_DATE = datetime.now()

# Magic numbers
MIN_IND = 2
AVG_IND = 1
MAX_IND = 0

# Process constants
RAND_RANGE_OFFSET = 10
ERRORS_RATE = 0.7
ERRORS_MIN_TERMINATE = 10
REFRESH_PROXY_MINS = 20
SECS_PER_MIN = 60
MAX_REPS_PER_URL = 3
DAYS_TO_FORECAST = 10
CALENDAR_START_YEAR = 2000
CALENDAR_END_YEAR = 2025

# Name constants
CURRENT_DATE = 'Current date'
PREVIOUS_DATE = 'Previous date'
PREVIOUS_DATE_PHARSE = 'Previous date phrase'
PREVIOUS_DATE_MAX_TEMP = 'Previous date max_temp'
PREVIOUS_DATE_MIN_TEMP = 'Previous date min_temp'
LATITUDE = 'latitude'
LONGITUDE = 'longitude'
LAT = 'lat'
LAT_X = 'lat_x'
LON = 'lon'
AIRPORT_INPUT_COL_NAME = 'code2'
STATE = 'state'
DATA = 'data'
CURRENT = 'current'
POLLUTION = 'pollution'
MESSAGE = 'message'
KEY = 'key'
STATION_NAME = 'station_name'
REGION = 'region'
COUNTRY = 'country'
YEAR_MONTH = 'Year-Month'
CITY = 'city'
CITY_ASCII = 'city_ascii'
REGION = 'region'
POLLUTION_AQIUS = 'pollution_aqius'
POLLUTION_AQICN = 'pollution_aqicn'
POLLUTION_MAINUS = 'pollution_mainus'
POLLUTION_MAINCN = 'pollution_maincn'
AQIUS = 'aqius'
AQICN = 'aqicn'
MAINUS = 'mainus'
MAINCN = 'maincn'
NAME = 'name'
ALPHA2CODE = 'alpha2Code'
REGION = 'region'
SUBREGION = 'subregion'
POPULATION = 'population'
AREA = 'area'
GINI = 'gini'
TEMPRATURE_SCALE = 'Temperature Scale'
AIRPORT_CODE = 'Airport Code'
HISTORY = 'History'
FORECAST = 'Forecast'
CITIES = 'Cities'
COUNTRIES = 'Countries'
CALENDAR = 'Calendar'
MAX_TEMP_MAX = 'max_temperature_max'
MAX_TEMP_MIN = 'max_temperature_min'
MAX_TEMP_AVG = 'max_temperature_avg'
AVG_TEMP_MAX = 'avg_temperature_max'
AVG_TEMP_MIN = 'avg_temperature_min'
AVG_TEMP_AVG = 'avg_temperature_avg'
MIN_TEMP_MAX = 'min_temperature_max'
MIN_TEMP_MIN = 'min_temperature_min'
MIN_TEMP_AVG = 'min_temperature_avg'
WIND_MAX = 'wind_max'
WIND_MIN = 'wind_min'
WIND_AVG = 'wind_avg'
GUST_WIND_MAX = 'gust_wind_max'
GUST_WIND_MIN = 'gust_wind_min'
GUST_WIND_AVG = 'gust_wind_avg'
SEA_LEVEL_PRESSURE_MAX = 'sea_level_pressure_max'
SEA_LEVEL_PRESSURE_MIN = 'sea_level_pressure_min'
SEA_LEVEL_PRESSURE_AVG = 'sea_level_pressure_avg'

# HTML Constants
DIV = 'div'
SPAN = 'span'
DAYS = 'days'
SUBHEADING = 'subheading'
H1 = 'h1'
CLASS_NAME_DATE = 'date ng-star-inserted'
CLASS_NAME_PHRASE = 'phrase ng-star-inserted'
CLASS_NAME_TEMPRATURE = 'temperature ng-star-inserted'
SVG = 'svg'
SUMMARY_TABLE = 'summary-table'
TR = 'tr'
TH = 'th'
TD = 'td'
CALENDAR_DAYS = 'calendar-days'
UL = 'ul'

# Error messages
MISSING_COLUMN_MSG = "'name2' column does not exist in file"
FILE_NOT_FOUND_MSG = "File does not exist"
EMPTY_COLUMN_MSG = 'Empty column was provided'
TOO_MANY_ERRORS_MSG = "Too many runtime errors, program will terminate..."
INPUT_ERROR_MSG_BROWSER = "Weather web-scraper input. Please insert the following:\n " \
                          "-type {Scraping module to use: History/Forecast/Calendar/Cities/Countries}\n" \
                          "--locations {Number of locations} - OPTIONAL \n" \
                          "-chrome_canary {Google chrome canary .exe path} - Only for History/Forecast Modules\n" \
                          "-chrome_driver {Google chrome driver .exe path} - Only for History/Forecast Modules\n" \
                          "-mysql_host {Local MySQL Host name} - DEFAULT = localhost \n" \
                          "-mysql_port {Local MySQL Port name} - DEFAULT = 3306 \n" \
                          "-mysql_pass {Local MySQL Password}\n" \
                          "-country {Country to fetch} - OPTIONAL (Use only for History/Forecast)\n" \
                          "-year_start {Fetching year range start} - OPTIONAL (Default = 2000)\n" \
                          "-year_end {Fetching year range end} - OPTIONAL (Default = current year)\n"
INPUT_ERROR_MSG_SQL = "\n-Missing SQL login input arguments. The following arguments are needed:\n--MySQL username\n" \
                      "--MySQL password"
PROXY_ERROR_MSG = 'Proxy list could not be retreived. Please check your connection to the internet.\n' \
                  'Program will terminate.'
UNKNOWN_FIELD_MSG = 'an unknown field was detected'
POLLUTION_DATA_MISSING_MSG = 'Pollution data missing'
PROCESS_TYPE_MSG = "Wrong process type. Please enter a valid value under '-type' parameter. " \
                   "Available process types are:\n-History\n-Forcast\n-Cities\n-Countries\n-Calendar"

# Input file Constants
INPUT_FILENAME = 'locations.csv'
ICAO_CODE = 'Icao_code'
INPUT_COUNTRIES_ZIP_CSV = 'worldcities.csv'

# Web constants
INPUT_COUNTRIES_ZIP = r'https://simplemaps.com/static/data/world-cities/basic/simplemaps_worldcities_basicv1.5.zip'
POLLUTION_API_ENDPOINT = r'http://api.airvisual.com/v2/nearest_city'
POLLUTION_API_KEY = '14bf8c48-2394-428d-aa95-20dd0918ef67'
COUNTRIES_ENDPOINT = r'https://restcountries.eu/rest/v2/all'

# SQL Constants
DB_Name = 'Weather'
CITIES_COLS_CONVERTER = {'code2': 'airport_code', 'country': 'country_code', 'name': 'station_name',
                         'region': 'region', 'lat': 'longitude', 'lon': 'latitude',
                         'pollution_aqius': 'pollution_rate_us', 'pollution_mainus': 'pollution_source_us',
                         'pollution_aqicn': 'pollution_rate_cn', 'pollution_maincn': 'pollution_source_cn'}
CITIES_AIRPORT_CODE = 'airport_code'
