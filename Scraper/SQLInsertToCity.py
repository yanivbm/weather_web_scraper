"""
This file contains the script to fill in SQL table cities.
"""

import sqlalchemy
import Constants


def insert_data_to_cities(df, mysql_host, mysql_user, mysql_pass, mysql_port):
    """
    This function takes a dataframe and converts it to a SQL data table
    """
    # Rename columns
    df = df.rename(columns=Constants.CITIES_COLS_CONVERTER)
    df = df.loc[:,list(Constants.CITIES_COLS_CONVERTER.values())].set_index(Constants.CITIES_AIRPORT_CODE)
    # Connect to DB
    engine = sqlalchemy.create_engine(
        f'mysql+pymysql://{mysql_user}:{mysql_pass}@{mysql_host}:{mysql_port}/{Constants.DB_Name}')
    # Append newly changed rows
    df.to_sql(name='cities', con=engine, if_exists='append')
