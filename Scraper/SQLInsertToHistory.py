"""
This file contains the script to fill in SQL table history.
"""

from datetime import date
import Constants as C


def insert_data_to_history(sample_dict, con, cur):
    """
    This function inserts the history data into a specific table
    :param sample_dict: dictionary of history data
    :param con: connection to the database where the table we need to populate is in
    :param cur: cursor
    """
    try:
        # Get the values form scrape dictionary
        calendar = date(int(list(sample_dict[C.YEAR_MONTH].split('-'))[0]),
                        int(list(sample_dict[C.YEAR_MONTH].split('-'))[1]), 1).strftime("%d-%m-%Y")
        max_temperature_max = sample_dict[C.MAX_TEMP_MAX]
        max_temperature_min = sample_dict[C.MAX_TEMP_MIN]
        max_temperature_avg = sample_dict[C.MAX_TEMP_AVG]
        airport_code = sample_dict[C.AIRPORT_CODE]
        avg_temperature_max = sample_dict[C.AVG_TEMP_MAX]
        avg_temperature_min = sample_dict[C.AVG_TEMP_MIN]
        avg_temperature_avg = sample_dict[C.AVG_TEMP_AVG]
        min_temperature_max = sample_dict[C.MIN_TEMP_MAX]
        min_temperature_min = sample_dict[C.MIN_TEMP_MIN]
        min_temperature_avg = sample_dict[C.MIN_TEMP_AVG]
        wind_max = sample_dict[C.WIND_MAX]
        wind_min = sample_dict[C.WIND_MIN]
        wind_avg = sample_dict[C.WIND_AVG]
        gust_wind_max = sample_dict[C.GUST_WIND_MAX]
        gust_wind_min = sample_dict[C.GUST_WIND_MIN]
        gust_wind_avg = sample_dict[C.GUST_WIND_AVG]
        sea_level_pressure_max = sample_dict[C.SEA_LEVEL_PRESSURE_MAX]
        sea_level_pressure_min = sample_dict[C.SEA_LEVEL_PRESSURE_MIN]
        sea_level_pressure_avg = sample_dict[C.SEA_LEVEL_PRESSURE_AVG]
        temperature_scale = sample_dict[C.TEMPRATURE_SCALE]

        sqlformula = """INSERT INTO history\
                    (date, airport_code, temperature_scale, temp_max_max, temp_max_min, temp_max_avg, temp_min_max,
                     temp_min_min,\
                    temp_min_avg, temp_avg_max, temp_avg_min, temp_avg_avg, windspeed_max, windspeed_min, windspeed_avg,\
                     gust_wind_max, gust_wind_min, gust_wind_avg, pressure_sealevel_max, pressure_sealevel_min,\
                     pressure_sealevel_avg)\
                     VALUES (STR_TO_DATE(%s,'%d-%m-%Y'), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                      %s, %s, %s, %s)
                     ON DUPLICATE KEY UPDATE
                     temperature_scale = %s,
                     temp_max_max = %s,
                     temp_max_min = %s,
                     temp_max_avg = %s,
                     temp_min_max = %s,
                     temp_min_min = %s,
                     temp_min_avg = %s,
                     temp_avg_max = %s,
                     temp_avg_min = %s,
                     temp_avg_avg = %s,
                     windspeed_max = %s,
                     windspeed_min = %s,
                     windspeed_avg = %s,
                     gust_wind_max = %s,
                     gust_wind_min = %s,
                     gust_wind_avg = %s,
                     pressure_sealevel_max = %s,
                     pressure_sealevel_min = %s,
                     pressure_sealevel_avg = %s,
                     last_update = NOW();
                     """

        history_row = (calendar, airport_code, temperature_scale, max_temperature_max, max_temperature_min,
                       max_temperature_avg,
                       min_temperature_max, min_temperature_min, min_temperature_avg, avg_temperature_max,
                       avg_temperature_min, avg_temperature_avg, wind_max, wind_min, wind_avg, gust_wind_max,
                       gust_wind_min,
                       gust_wind_avg, sea_level_pressure_max, sea_level_pressure_min, sea_level_pressure_avg,
                       temperature_scale, max_temperature_max, max_temperature_min, max_temperature_avg,
                       min_temperature_max, min_temperature_min, min_temperature_avg, avg_temperature_max,
                       avg_temperature_min, avg_temperature_avg, wind_max, wind_min, wind_avg, gust_wind_max,
                       gust_wind_min,
                       gust_wind_avg, sea_level_pressure_max, sea_level_pressure_min, sea_level_pressure_avg)

        cur.execute(sqlformula, history_row)
        con.commit()
        return True
    except:
        return False
