"""
This file contains the script to create database into the SQL server.
"""

import SQLConnect
import Constants as C


def create_sql_db(host, user, password):
    """
    This function connects to DATABASE SERVER and creates Database named 'weather' if not exists
    """
    # Connect to DATABASE SERVER
    con, cur = SQLConnect.connect_to_server(host, user, password)

    # Create database
    cur.execute("CREATE DATABASE IF NOT EXISTS {}".format(C.DB_Name))
    con.commit()
    cur.close()
