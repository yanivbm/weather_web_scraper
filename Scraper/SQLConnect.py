"""
This file contains the script to connect to the SQL server and to SQL database.
"""

import mysql.connector


# Connect to SQL SERVER
def connect_to_server(my_host, my_user, my_password):
    """
    This function makes the connection to the host SQL server
    :param my_host: host name
    :param my_user: username
    :param my_password: password
    """
    con = mysql.connector.connect(
        host=my_host,
        user=my_user,
        passwd=my_password
    )
    cur = con.cursor()
    return con, cur


# Connect to DATABASE
def connect_to_database(my_host, my_user, my_password, my_database):
    """
    This function makes the connection to the database
    :param my_host: host name
    :param my_user: username
    :param my_password: password
    :param my_database: database name
    """
    con = mysql.connector.connect(
        host=my_host,
        user=my_user,
        passwd=my_password,
        database=my_database
    )
    cur = con.cursor()
    return con, cur
