"""
This file contains the scrape_weather_history script of Weather History web-scraping (wunderground.com).
The script calls related functions and runs the scrape_weather_history loop that iterates over all locations & dates.
"""

# Import other tools
import logging

# Import other project files
import Constants
from Scraper import Scraper
from Webpage import Webpage
from History import History
import WeatherScraperUtils
import SQLInsertToHistory
import SQLConnect

logging.disable(logging.CRITICAL)


def scrape_weather_history(parser, args):
    """
    This function manages the process and calls other functions that perform the scraping.
    This function calls functions that do the following: get proxy list, read locations from a csv file,
    init http GET request and scrape data from the html.
    """
    # Parse input parameters
    (chrome_driver_path, chrome_canary_path, locations_amt, mysql_host, mysql_port, mysql_user,
     mysql_pass, country_to_fetch, year_start, year_end) = \
        WeatherScraperUtils.validate_input_main(parser, args, browser=True, sql=True)

    # Init DB
    WeatherScraperUtils.init_db(mysql_host, mysql_user, mysql_pass)

    # Init Scraper object
    scraper = Scraper(country_to_fetch)

    # Retrieve available proxies list
    scraper.get_proxy_list()

    # Read input fle
    scraper.read_input_file()

    # connect to database
    con, cur = SQLConnect.connect_to_database(mysql_host, mysql_user, mysql_pass, Constants.DB_Name)
    try:
        # Iterate over airports list and get the all monthly htmls for each location
        for ind, airport in enumerate(scraper.airport_list[:locations_amt]):

            scraper.check_status(ind)
            url = 'https://www.wunderground.com/history/monthly/{}/date/{}-{}'.format(airport, year_start, 1)
            req = Webpage(url, airport, year_start, year_end)

            # Init Webpage object
            if not req.access_url(scraper, airport, chrome_canary_path, chrome_driver_path):
                continue
            req.get_source_history(scraper)

            # Scrape details from HTML
            for ym_source in sorted(req.page_source_data_table.keys()):
                hist = History(ym_source, req.page_source_data_table[ym_source], req.airport_code)
                if not WeatherScraperUtils.read_url_main(scraper, hist, airport):
                    break
                # Update SQL table
                SQLInsertToHistory.insert_data_to_history(hist.data, con, cur)

        scraper.print_summary()
    finally:
        con.commit()
        cur.close()
