Yaniv Ben-Malka
Jacky Lalou

In order to run the program, run WeatherScraperMain.py file, with a proper input (Defined under 'REQUIRED INPUTS')

***************************************  PREVIEW   ***************************************

End to End Worldwide Weather Data Web-Scraper.
Data is scraped from wunderground.com.
The follwoing data is scraped:
- Historical monthly summary reports
- Ongoing weekly forecast. 
The scraped data is stored in a MySQL database.
Reqests overload is taken into account, requests are sent in a reasonable pace.

***************************************  PYTHON VERSION ***************************************

Pyhton 3.6

***************************************  USED LIBRARIES  ***************************************

* See requirements.txt file

***********************************  PRE-REQUISIT DOWNLOADS  ***********************************

Pre-requisit downloads:
Google Chrome Canary  (https://www.google.com/intl/fr/chrome/canary/)
ChromeDriver 79.0.3945.16  (https://chromedriver.chromium.org/downloads)

*Module can work with regular Google Chrome as well (With a version-compatible driver)

**************************************  REQUIRED INPUTS  **************************************

Inputs to be provided through CLI:

-type {Scraping module to use: History/Forecast/Calendar/Cities/Countries}
-locations {Number of locations} - OPTIONAL
-chrome_canary {Google chrome canary .exe path} - Only for History/Forecast Modules
-chrome_driver {Google chrome driver .exe path} - Only for History/Forecast Modules
-mysql_host {Local MySQL Host name} - DEFAULT = localhost
-mysql_port {Local MySQL Port name} - DEFAULT = 3306 
-mysql_pass {Local MySQL Password}
-country {Country to fetch} - OPTIONAL (Use only for History/Forecast)
-year_start {Fetching year range start} - OPTIONAL (Default - 2000)
-year_end {Fetching year range end} - OPTIONAL (Default - current year)

Input Example:

-type Forecast -chrome_driver "C:\Program Files\ChromeDriver\chromedriver78.exe" -chrome_canary "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" -locations_amt 4 -mysql_host localhost -mysql_user root -mysql_pass 12345! -year_start 2015 -year_end 2016 -country Iceland

IMPORTANT NOTE:

Cities/Countries/Calendar type (CLI parameter -type) - NEEDS TO BE RUN ONLY INITIALLY.
These tables will be generated in the database and will be ready for further use.
These tables are not needed to be updated on a regular basis.

**************************************  FILES LIST  **************************************

- WeatherScraperMain.py (Main file that calls all other modules)
- Constants.py (Constants file)
- CitiesScraper.py (cities table Population script)
- DailyForecastScraper.py (Forecast table Population script)
- WeatherHistoryScraper.py (History table Population script)
- Scraper.py (Scraper Class)
- History.py (History Class)
- Forecast.py (Forecast Class)
- Webpage.py (Webpage Class)
- WeatherScraperUtils.py (General functions that support the scraping process)
- SQLCreateDB.py (Create SQL database)
- SQLCreateTables.py (Create SQL tables)
- SQLConnect.py (Connect to SQL database)
- SQLInsertToHistory.py (Inserting a line to History table)
- SQLInsertToForecast.py (Updating weekly forecast of certain day in Forecast table)
- SQLInsertToCountries.py (Populating countries table)
- SQLInsertToCity.py (Populating City table - THE WHOLE TABLE IS PUSHED IN ONE QUERY)
- SQLInsertToCalendar.py (Populating calendar table)
- Weather history (Scraping weather history)
- requirments.txt (Required packages file)
- README.txt (Required packages file)
- WebScraping_project.ppt (Project description)
- locations.csv (Locations input file)
- DB_Scheme.png (DB Scheme)